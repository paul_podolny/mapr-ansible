Set credentials:
----------------
export AWS_ACCESS_KEY="xxxx"
export AWS_SECRET_KEY="xxxxxxxxxx"
export AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY"
export AWS_SECRET_ACCESS_KEY="$AWS_SECRET_KEY"
 
Provosion nodes:
----------------
```
 ansible-playbook -u ec2-user --private-key ~/.ssh/<my-key-name>.pem -i hosts playbooks/aws_bootstrap.yml -e key_name="<key-name>"
```
Install cluster:
----------------
```
  ansible-playbook -u ec2-user --private-key ~/.ssh/<my-key-name>.pem -i playbooks/cluster.hosts playbooks/install_cluster.yml
```
