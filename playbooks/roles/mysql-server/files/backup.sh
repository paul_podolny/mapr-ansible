#!/usr/bin/env bash

set -e
set -u

lockfile="/tmp/mysqlbackup.lock"
backup_path="/tmp/backup"
backup_user="root"
backup_pass="mapr"
ttl="3"

backup() {
  if [ ! -d ${backup_path} ] ;then
    mkdir -p ${backup_path} || echo "cannot write to ${backup_path}" && exit 1
  fi
  mysqldump -u ${backup_user} -p${backup_pass} --all-databases | bzip2  > ${backup_path}/mysql_backup_`date +%F`.sql.bz2
}

retention() {
  find ${backup_path} -name "mysql_backup*.bz2" -type f -mtime +${ttl} | xargs \rm -f
}

if [ ! -e $lockfile ]; then
   trap "rm -f $lockfile; exit" INT TERM EXIT
   touch $lockfile
   backup && retention
   rm $lockfile
   trap - INT TERM EXIT
else
   echo "backup is already running"
fi
